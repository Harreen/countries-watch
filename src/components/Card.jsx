
const Card = (props) => {
  const { country } = props;

  const numberFormat = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
  }

  return (
    <div>
      <ul>
        <li key={country.name} className="card">
          <img src={country.flag} alt={country.name} />
          <div className="data-container">
            <ul>
              <li>{country.name}</li>
              <li>Capital : {country.capital}</li>
              <li>Pop. {numberFormat(country.population)}</li>
            </ul>
          </div>
        </li>
      </ul>
    </div>
  );
};

export default Card;