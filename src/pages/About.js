

const About = () => {
  return (
    <div className="about">
      <h1>About</h1>
      <br />
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla egestas hendrerit aliquet. Sed lectus arcu, pretium a dolor vel, tempor mollis odio. Integer a elementum elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque dui ligula, ullamcorper at tellus eu, sodales feugiat ex. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec at cursus elit. Curabitur at faucibus turpis, at sagittis nisi.
      </p>
      <br />
      <p>
        Excepteur eu adipisicing non voluptate aliquip dolor aliqua adipisicing ad magna fugiat cillum sunt. Eu ullamco fugiat sit occaecat ut qui quis ea aliqua. Nostrud irure sint aliquip excepteur nulla do reprehenderit ad excepteur exercitation ut anim et. Voluptate nostrud qui excepteur voluptate non non laboris laborum laborum voluptate ut eiusmod. Culpa pariatur laboris est adipisicing ad non consequat. Mollit aliquip irure irure ipsum mollit eu duis Lorem ut.
        Eu cillum aliquip exercitation laboris. Aute culpa laborum enim nisi cupidatat sit fugiat excepteur. Amet nostrud occaecat aute quis ex quis. Sunt officia sit esse ad irure aliqua cillum nulla in aliqua magna. Irure in amet ullamco incididunt. Anim commodo reprehenderit minim laborum. Cupidatat incididunt nulla ut exercitation sit labore.
      </p>
    </div>
  );
};

export default About;