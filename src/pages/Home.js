import Countries from "../components/Countries";

const Home = () => {
  return (
    <div className="countries">
      <Countries />
    </div>
  )
}

export default Home;